package ru.demaRobot;

public enum Direction {
    UP,
    DOWN,
    RIGHT,
    LEFT;
}
